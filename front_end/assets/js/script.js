/**
 * @file
 *
 * Tidy Local Tasks custom JS.
 */

(function (Drupal) {
  "use strict";
  Drupal.behaviors.tidyLocalTasks = {
    attach: function (context, settings) {
      const tltMenu = once('tlt-menu', document.querySelector('.tlt-menu'));
	  tltMenu.forEach(function (el) {
        const arrow = el.querySelector('.show-hide'),
        themeLocalTasksToggle = el.querySelector('.tabs__trigger');
        arrow.addEventListener('click', (e) => {
          el.classList.toggle("active");
        });

        // Olivero, at least, provides for local tasks to collapse into an
        // expandable menu at mobile widths. We're taking care of that
        // ourselves, so here we remove the toggle that they provide.
        if (themeLocalTasksToggle) {
          themeLocalTasksToggle.remove();
        }
	  });
    },
  };
})(Drupal);
