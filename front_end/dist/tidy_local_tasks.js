!function(e){"use strict";Drupal.behaviors.tidyLocalTasks={attach:function(e,t){once("tlt-menu",document.querySelector(".tlt-menu")).forEach((function(e){const t=e.querySelector(".show-hide"),c=e.querySelector(".tabs__trigger");t.addEventListener("click",(t=>{e.classList.toggle("active")})),c&&c.remove()}))}}}();
//# sourceMappingURL=tidy_local_tasks.js.map
